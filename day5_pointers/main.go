package main

import (
	"fmt"
)

// To understand how the pointers work
func main() {
	var a = 10
	var a1 = 20
	var b *int
	b = &a
	fmt.Println("Address of a is :", b)
	fmt.Println("Value at the address of a is :", *b)
	sumByValue := sumByValue(a, 20)
	fmt.Println("SumByValue is :", sumByValue)
	sumByValueWithBonus := sumByValueWithBonus(a, 20)
	fmt.Println("sumByValueWithBonus is :", sumByValueWithBonus)
	fmt.Println("Value of a is:", a)
	sumByReferenceWithBonus := sumByReferenceWithBonus(&a, &a1)
	fmt.Println("sumByReferenceWithBonus is :", sumByReferenceWithBonus)
	fmt.Println("Value of a is:", a)
}

func sumByValue(v1 int, v2 int) int {
	sum := v1 + v2
	return sum
}

func sumByValueWithBonus(v1 int, v2 int) int {
	v1 = 100
	sum := v1 + v2
	return sum
}

func sumByReferenceWithBonus(v1 *int, v2 *int) int {
	*v1 = 100
	sum := *v1 + *v2
	return sum
}
