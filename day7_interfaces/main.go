// Understanding interfaces
package main

import (
	"log"
)

//Shape implements the area()
type Shape interface {
	area() int
}

//Square implement square !!
type Square struct {
	side int
}

func (s Square) area() int {
	return s.side * s.side
}

//Rectange implement square !!
type Rectange struct {
	length, breadth int
}

func (r Rectange) area() int {
	return r.length * r.breadth
}

func main() {
	// You can do this to print the area
	sq := Square{5}
	log.Println("Area is :", sq.area())
	rec := Rectange{4, 5}
	log.Println("Area is :", rec.area())

	// Passing the interface as arguments to functions is the main use of interface.
	sq = Square{5}
	printArea(sq)
	printArea(Rectange{4, 5})

	// One more example where I am passing interace as argument
	totalSum := totalArea(sq, rec)
	log.Println("totalSum: ", totalSum)
}

func printArea(s Shape) {
	log.Println("Area is :", s.area())
}

//A variadic function (takes multiple arguments of same type) taking interface argument
func totalArea(shapes ...Shape) int {
	total := 0
	for _, shape := range shapes {
		total += shape.area()
	}
	return total
}
