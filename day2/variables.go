package main

import "fmt"

func main() {
	var a = "first"
	fmt.Println(a)
	b := "first"
	fmt.Println(b)
	c := 1
	fmt.Println(c)

	var i = 5
	//i := 5
	for i <= 0 {
		i = i - 1
		fmt.Println(i)
	}

	num := 9

	if  num < 0 {
        fmt.Println(num, "is negative")
    } else if num < 10 {
        fmt.Println(num, "has 1 digit")
    } else {
        fmt.Println(num, "has multiple digits")
	}
	
	fmt.Println(num)
	
}
