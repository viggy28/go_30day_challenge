package main

import "fmt"

func main() {

	// Arrays
	var a [5]int
	a[1] = 100

	fmt.Println(a)

	b := [5]int{1,2,3,4,5}
	fmt.Println(b[2])
	// Slice
	var c = make([]int,2)
	c[0] = 2
	//c[1] = 3
	c = append(c,3)
	fmt.Println(c)

	m := make(map[string]int)

	m["k2"] = 13
	_, prs := m["k2"]
	fmt.Println("prs:", prs)
	fmt.Println("value:", value)
}