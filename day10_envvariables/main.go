package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

// TwilioEnv To set the env variables related to Twilio
type TwilioEnv struct {
	AccountSid string
	AuthToken  string
}

func (t *TwilioEnv) setEnv() {
	if t.AccountSid = os.Getenv("TWILIOSMSACCOUNTSID"); t.AccountSid != "" {
		log.Println("AccountSid:", t.AccountSid)
	}
	if t.AuthToken = os.Getenv("TWILIOSMSAUTHTOKEN"); t.AuthToken != "" {
		log.Println("AuthToken set:", t.AuthToken)
	}
	if t.AccountSid == "" || t.AuthToken == "" {
		t.AccountSid = "123"
		t.AuthToken = "abc"
	}
}

// ServeHTTP
func (t TwilioEnv) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	t.setEnv()
	w.Write([]byte("Send SMS Done"))
}

func main() {
	fmt.Println("Inside Main")
	router := mux.NewRouter()
	port := "8080"
	twilio := TwilioEnv{}
	router.HandleFunc("/", welcomeMessage).Methods("GET")
	router.HandleFunc("/sms", twilio.ServeHTTP).Methods("GET")
	http.ListenAndServe(fmt.Sprintf(":%s", port), twilio)
}

func welcomeMessage(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Welcome to env api"))
}
