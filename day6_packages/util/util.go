package util

import (
	"errors"
	"reflect"
	"strconv"
)

//IntToString converts a given integer to string
func IntToString(v1 int) (string, error) {
	typeOf := reflect.TypeOf(v1)
	if typeOf.Name() != "int" {
		return "", errors.New("Invalid type. Only Int is accepted")
	}
	return strconv.Itoa(v1), nil
}
