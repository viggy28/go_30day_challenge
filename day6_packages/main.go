package main

import (
	"log"

	"gitlab.com/vr001/go_30day_challenge/day6_packages/util"
)

func main() {
	stringValue, err := util.IntToString(4)
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	log.Println("Int to String " + stringValue)
}
