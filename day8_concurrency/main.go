package main

import (
	"fmt"
	"time"
)

/* func pinger(c chan interface{}) {
	for i := 0; i < 1; i++ {
		c <- "ping"
	}
}

func ponger(c chan interface{}) {
	for i := 0; i < 1; i++ {
		c <- 200
	}
}

func printer(c chan interface{}) {
	for {
		msg := <-c
		fmt.Println(msg)
		time.Sleep(time.Millisecond * 1)
	}
} */

func main() {
	ch1 := make(chan string)
	//var c = make(chan interface{})
	/* go pinger(c)
	go ponger(c)
	go printer(c) */
	go func() {
		fmt.Println("Begin f1 ")
		//time.Sleep(1000 * time.Millisecond)
		ch1 <- "hello"
		fmt.Println("End f1")
	}()
	go func() {
		fmt.Println("Begin f2 ")
		time.Sleep(3000 * time.Millisecond)
		//val := <-ch1
		//fmt.Println(val)
		fmt.Println("End f2")
	}()
	var input string
	fmt.Scanln(&input)
}

/* package main

import (
	"errors"
	"fmt"
	"time"
)

func sum(s []int, c chan error) {
	sum := 0
	for _, v := range s {
		sum += v
	}
	time.Sleep(time.Millisecond * 1)
	c <- errors.New("error") // send sum to c
}

func main() {
	s := []int{7, 2, 8, -9, 4, 0}

	c := make(chan error)
	go sum(s[:len(s)/2], c)
	go sum(s[len(s)/2:], c)

	x, y := <-c, <-c // receive from c

	fmt.Println(x, y)
	fmt.Println("Hellow")
}
*/
